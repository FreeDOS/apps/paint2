uses Crt, paint2u{, execswap};

function Exist(S : String) : Boolean;
var
 f : file;
Begin
   assign(f,s);
   {$i-}reset(f);{$i+}
   if IOResult <> 0 then
    Exist := false
   else
   Begin
      Exist := true;
      close(f);
   End;
End;

Procedure perror(str:string);
Begin
   Writeln;
   Writeln(str);
   Writeln;
   Writeln('USAGE: CONVERT [.YMG File] [New data file] (/obj)');
   Halt;
End;

var
 f      : file;
 f2     : file;
 IMG    : YMGHeader;
 x1, y1 : Integer;
 P      : Pointer;
 P1, P2 : String;
 OBJ    : boolean;
 S      : String;
 pf     : text;
 Ch     : Char;

Begin
   {Error cheking...}
   if ParamCount < 2 then
      perror('Error #1: not enough parameters');

   if ParamCount = 3 then
   Begin
      if (ParamStr(3) <> '/obj') and (ParamStr(3) <> '/OBJ') then
        perror('Error #3: invalid switch: "'+paramstr(3)+'"');

      obj := true;
   End;

   P1 := ParamStr(1);
   P2 := ParamStr(2);
   if not exist(p1) then
      perror('Error #2: The file: "'+p1+'" doesn''t exist.');

   {------------------}
   Assign(F,P1);
   Reset(F,1);
   BlockRead(F,IMG,SizeOf(IMG));
   x1 := IMG.Px;
   y1 := IMG.Py;
   GetMem(P,(x1-2)*(y1-18));
   BlockRead(F,P^,(x1-1)*(y1-17));
   Close(F);

   Assign(F2, P2);
   {$i-}Rewrite(F2,1);{$I+}
   if IOResult <> 0 then
    perror('Error #4: "'+p2+'" - file access denied!');
   {$i-}BlockWrite(F2,P^,(x1-1)*(y1-17));{$i+}
   if IOResult <> 0 then
    perror('Error #4: "'+p2+'" - file access denied!');
   {$i-}Close(F2);{$i+}
   if IOResult <> 0 then
    perror('Error #5: "'+p2+'" - file cannot be closed!');

   Writeln;
   Writeln('CONVERT- CopyRight(c) 1998 - Y.M.S Crop.');
   Writeln('Programming by Yotam Madem');
   Writeln;
   Writeln('YMG file "',p1,'" converted successfuly to');
   Writeln('A DATA file "',p2,'".');
   Writeln('YMG picture size: ',x1,'x',y1);

   if obj then
   Begin
      Writeln;
      Writeln('Executing the Borland BINOBJ convertor...');
      Write('Enter data name: ');
      Readln(s);
      {ExecProg('BINOBJ '+p1+' '+s+'.obj '+s);}
      Writeln;
      Writeln('Cheking BINOBJ work...');
      if exist(s+'.obj') then
      Begin
         Writeln('BINOBJ has finished his work successfuly.');
         Writeln('The file '+s+'.obj'+' has been created');
         Writeln('You can use the file data on this way: ');
         Writeln;
         Writeln('uses Crt, paint2u, vga2;');
         Writeln;
         Writeln('Procedure ',s,'; external;');
         Writeln('{$L ',s,'.obj}');
         Writeln;
         Writeln('begin');
         Writeln('   SetMCGA;');
         Writeln('   Draw_YMGP(@',s,',1,1,',x1,',',y1,');');
         Writeln('   Readkey;');
         Writeln('   SetText;');
         Writeln('end.');
         Writeln;
         Write('Do you want to print it to a file?');
         repeat
            Ch := ReadKey;
            Ch := UpCase(Ch);
         until (Ch = 'Y') or (Ch = 'N');

         if Ch = 'Y' then
         Begin
            assign(pf, s+'.pas');
            rewrite(pf);
            Writeln(pf,'uses Crt, paint2u, vga2;');
            Writeln(pf);
            Writeln(pf,'Procedure ',s,'; external;');
            Writeln(pf,'{$L ',s,'.obj}');
            Writeln(pf);
            Writeln(pf,'begin');
            Writeln(pf,'   SetMCGA;');
            Writeln(pf,'   Draw_YMGP(@',s,',1,1,',x1,',',y1,');');
            Writeln(pf,'   Readkey;');
            Writeln(pf,'   SetText;');
            Writeln(pf,'end.');
            close(pf);
         End;
      End
     else
      Begin
         Writeln('BINOBJ DID NOT finish his work successfuly.');
      End;

   End;
End.